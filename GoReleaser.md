# GoReleaser

I'm going to use this repository to integrate and replace my Makefile with GoReleaser

## Initial state

To automate some of the steps, I'm using a good ol' Makefile

To compile this software in amd64, I just have to run:

```
VERSION=0.0.1 make build
```

To create a docker image for this, I need to run:

```
TAG=0.0.1 make dockerbuild
```

To push the newly created docker image to hub.docker.com, I need to run:

```
TAG=0.0.1 make dockerpush
```

That's better than doing `go build`s, `docker build`s and `docker push`es by hand, but not by much...

## Enter GoReleaser

[GoReleaser](https://goreleaser.com/) is an open source software that aims to automate most boring tasks from shipping go software. 

> With GoReleaser, you can:
>
>    Cross-compile your Go project
>    Release to GitHub, GitLab and Gitea
>    Create nightly builds
>    Create Docker images and manifests
>    Create Linux packages and Homebrew taps
>    Sign artifacts, checksums and container images
>    Announce new releases on Twitter, Slack, Discord and others
>    Generate SBOMs (Software Bill of Materials) for binaries and container images
>    ... and much more!

There is a Pro version that supports the developer, but we will only use the opencore one in this document.

## 0. Initialize GoReleaser

You can generate a basic working GoReleaser config file with the command

```bash
$ goreleaser init
  • Generating .goreleaser.yaml file
  • config created; please edit accordingly to your needs file=.goreleaser.yaml
  • thanks for using goreleaser!
```

This file is a good start but for the sake of the demonstration, I'll start with my own simpler file first.

## 1. Build

I'm going to replace the first parts of the Makefile

```makefile
prepare:
	go mod tidy

build: prepare
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o bin/tifling -ldflags "-X main.Version=$$VERSION" main.go
```

With GoReleaser, it should look like this

```yaml
before:
  hooks:
    - go mod tidy

builds:
  - binary: bin/tifling
    env:
      - CGO_ENABLED=0
    goos:
      - linux
    goarch:
      - "amd64"
    ldflags:
      - -X main.Version=={{.Version}}
```

Section before.hooks will allow us to do some things before building, just like we did with the "build: prepare" line in the Makefile.

`{{.Version}}` is an automated variable from GoReleaser. We'll use it to get rid of the manual task of setting the `$$VERSION` and `$$TAG` from our Makefile. GoReleaser is going to leverage git tags for this.

Note: there are many variables and they will be super useful in many usecases. 

Let's now commit our new .goreleaser.yml file, add a git tag:

```
git add .
git commit -m "add simplest goreleaser example"
git tag -a 0.0.1 -m "simplest goreleaser example"
git push origin 0.0.1
```

And then run goreleaser

```
$ goreleaser build --clean
  • starting build...
  • loading                                          path=.goreleaser.yaml
  • loading environment variables
  • getting and validating git state
    • couldn't find any tags before "0.0.1"
    • git state                                      commit=2778c1e356e1b474ca9fe3081e450a3bd8a430e3 branch=main current_tag=0.0.1 previous_tag=<unknown> dirty=false
  • parsing tag
  • setting defaults
  • running before hooks
    • running                                        hook=go mod tidy
  • checking distribution directory
  • loading go mod information
  • build prerequisites
  • writing effective config file
    • writing                                        config=dist/config.yaml
  • building binaries
    • building                                       binary=dist/tifling_linux_amd64_v1/bin/tifling
  • storing release metadata
    • writing                                        file=dist/artifacts.json
    • writing                                        file=dist/metadata.json
  • build succeeded after 0s
  • thanks for using goreleaser!
```

We can now check if our binary is functional, and version is automatically set by goreleaser using the git tag:

```
$ dist/tifling_linux_amd64_v1/bin/tifling
## tifling version =0.0.1

Random Entry:
- Name: Coiff'hair
- Latitude/Longitude: 48.556123 -2.019321
```

Should we try to execute goreleaser without tagging a branch first, we will get an error:

```
  ⨯ build failed after 0s                    error=git is in a dirty state
```

or 

```
  ⨯ build failed after 0s                    error=git tag 0.0.1 was not made against commit 4a03b27dd1369cbeb2451d362493756fc90d7d48
```

This is normal, goreleaser relies on tags to work. In case we really want to use goreleaser and not tag the code, we call use the `--snapshot` option. This will be useful for snapshot/nightly releases for example:

```
$ goreleaser build --clean --snapshot
[...]
  • snapshotting
    • building snapshot...                           version=0.0.1-SNAPSHOT-4a03b27
[...]

$ dist/tifling_linux_amd64_v1/bin/tifling
## tifling version =0.0.1-SNAPSHOT-4a03b27
```

## 2. Crossbuild

Now that we reproduced the basic functionality of the initial Makefile, we can try to go further.

Rather than limiting our build to amd64 on linux only, we can remove the `goos` and `goarch` sections completely to tell goreleaser to crossbuild to the most common archs. We also can keep those and explicitly define those we want.

```diff
before:
  hooks:
    - go mod tidy

builds:
  - env:
      - CGO_ENABLED=0
-    binary: bin/tifling
-    goos:
-      - linux
-    goarch:
-      - "amd64"
    ldflags:
      - -X main.Version=={{.Version}}
```

If we try to run goreleaser again, we will build much our binary for many more targets out of the box, which the need to write a loop in the Makefile or in a script:

```
git add .
git commit -m "goreleaser add more targets"
git tag -a 0.0.2 -m "goreleaser add more targets"
git push origin 0.0.2

$ goreleaser build --clean
[...]

$ ls dist
artifacts.json  tifling_darwin_amd64_v1  tifling_linux_amd64_v1  tifling_windows_amd64_v1
config.yaml     tifling_darwin_arm64     tifling_linux_arm64     tifling_windows_arm64
metadata.json   tifling_linux_386        tifling_windows_386
```

## 3. Push the release

Now, We are going to go farther than what our Makefile does and automatically create a gitlab.com release for the tagged version of our code. The first thing to do is to create a [GitLab token here](https://gitlab.com/-/profile/personal_access_tokens). Click "Add a new token", give it a name, an expiration date, and the scope **write_repository**.

Then, create a file called `~/.config/goreleaser/gitlab_token` which contains ONLY the token:

```
aaaaa--aaaaaa_aaaaaaaaaaaa
```

Note: this can works for the public GitLab, but also private ones, and also GitHub and Gitea (see [scm in goreleaser official documentation](https://goreleaser.com/scm/gitlab/)). 

Now, to create a proper release for our project, we are going to add archives and checksums as artefacts of the release. To do this, we will add this in our .goreleaser.yml file:

```diff
before:
  hooks:
    - go mod tidy

builds:
  - env:
      - CGO_ENABLED=0
    ldflags:
      - -X main.Version=={{.Version}}

+archives:
+  - format: tar.gz
+    format_overrides:
+      - goos: windows
+        format: zip
+
+checksum:
+  name_template: "{{ .ProjectName }}_checksums.txt"
```

Let's now push our release to gitlab.com!

```
git add .
git commit -m "goreleaser add github release"
git tag -a 0.0.3 -m "goreleaser add github release"
git push origin 0.0.3
```

Now, we are going to stop using the `goreleaser build` command and use `goreleaser` (without "build") to not only build the software but also create the release:

```
goreleaser --clean
[...]
  • publishing
    • scm releases
      • creating or updating release                 tag=0.0.3 repo=
      • refreshing checksums                         file=tifling_checksums.txt
      • release created                              name=0.0.3
      • uploading to release                         file=dist/tifling_0.0.3_darwin_amd64.tar.gz name=tifling_0.0.3_darwin_amd64.tar.gz
      • uploading to release                         file=dist/tifling_0.0.3_linux_amd64.tar.gz name=tifling_0.0.3_linux_amd64.tar.gz
      • uploading to release                         file=dist/tifling_0.0.3_windows_amd64.zip name=tifling_0.0.3_windows_amd64.zip
      • uploading to release                         file=dist/tifling_0.0.3_windows_386.zip name=tifling_0.0.3_windows_386.zip
      • uploading to release                         file=dist/tifling_0.0.3_linux_386.tar.gz name=tifling_0.0.3_linux_386.tar.gz
      • uploading to release                         file=dist/tifling_0.0.3_darwin_arm64.tar.gz name=tifling_0.0.3_darwin_arm64.tar.gz
      • uploading to release                         file=dist/tifling_0.0.3_windows_arm64.zip name=tifling_0.0.3_windows_arm64.zip
      • uploading to release                         file=dist/tifling_0.0.3_linux_arm64.tar.gz name=tifling_0.0.3_linux_arm64.tar.gz
      • uploading to release                         file=dist/tifling_checksums.txt name=tifling_checksums.txt
      • published                                    url=https://gitlab.com/dt.germain/tifling/-/releases/0.0.3
      • took: 6s
  • took: 6s
[...]
```

## 4. Build a docker image

There is a part that we put aside for now in the Makefile. The part building and pushing the docker image:

```makefile
dockerbuild:
	docker build -t zwindler/tifling:$$TAG --build-arg VERSION=$$TAG . && docker build -t zwindler/tifling:latest --build-arg VERSION=$$TAG .

dockerpush: dockerbuild
	docker push zwindler/tifling:$$TAG && docker push zwindler/tifling:latest
```

Of course, this part can also be automated as well.

The first thing we are going to do is to remove a part of the Dockerfile. The Dockerfile had been created as a mutli-stage image. The first stage, called "builder", is an image that is only used to do the build itself. Then, we create a new "scratch" image containing only the binary created the step before.

Since goreleaser is responsible for building the golang binary, we are going to remove everything related to the builder image.

```diff
-FROM golang:1.21-alpine AS builder
-ARG VERSION
-
-WORKDIR /bin
-
-COPY / .
-RUN apk add build-base && go mod download && go build -o tifling \
-    -ldflags "-X main.Version=$VERSION" main.go

FROM scratch

-COPY --from=builder /bin/tifling /
+COPY tifling /

CMD [ "/tifling" ]
```

Much more simple :\)

Then, we are going to tell goreleaser to build and then push the image to hub.docker.com

```diff
before:
  hooks:
    - go mod tidy

builds:
  - env:
      - CGO_ENABLED=0
    ldflags:
      - -X main.Version=={{.Version}}

+dockers:
+  - image_templates:
+      - "zwindler/{{.ProjectName}}:{{.Version}}-amd64"
+      - "zwindler/{{.ProjectName}}:latest-amd64"
+    build_flag_templates:
+      - "--platform=linux/amd64"
+      - "--build-arg=VERSION={{.Version}}"

archives:
  - format: tar.gz
    format_overrides:
      - goos: windows
        format: zip

checksum:
  name_template: "{{ .ProjectName }}_checksums.txt"
```

In this example, we will only build, tag and push a docker image for linux/amd64, but this is configurable using goos/goarch. You can also use buildx for crossbuilds (see [documentation](https://goreleaser.com/customization/docker/#use-a-specific-builder-with-docker-buildx))


```
git add .
git commit -m "goreleaser add docker build stage"
git tag -a 0.0.4 -m "goreleaser add docker build stage"
git push origin 0.0.4
```

Note: you have to manually login before launching goreleaser

```
docker login
goreleaser --clean
[...]
  • docker images
    • building docker image                          image=zwindler/tifling:0.0.4-amd64
    • took: 5s
  • publishing
    • docker images
      • pushing                                      image=zwindler/tifling:0.0.4-amd64
      • pushing                                      image=zwindler/tifling:latest-amd64
      • took: 13s
[...]
```

## 5. Announce

When project release a new version, it's always nice to have some kind of automated system to tell the world.

Since Twitter (X) has closed the API, we are going to publish the exciting news of our release to Mastodon!

Create an app https://social.yourdomain.tld/settings/applications/new (in my case https://framapiaf.org/settings/applications/new) with `write:statuses` permission only

Once created, get the ID/Secret/Token from it, and set them in the following env vars:
* MASTODON_CLIENT_ID: "Client key".
* MASTODON_CLIENT_SECRET: "Client secret".
* MASTODON_ACCESS_TOKEN: "Your access token".

Finally, add the announce part in your goreleaser config file

```
announce:
  mastodon:
    enabled: true
    server: https://framapiaf.org
```

And tag + run goreleaser

```
git add .
git commit -m "goreleaser add mastodon announcement"
git tag -a 0.0.5 -m "goreleaser add mastodon announcement"
git push origin 0.0.5
```

Note: you have to set the env variables in the .goreleaser.yaml or set them before running goreleaser. In order to avoid pushing sensitive information to git, I wrote a local script for this but we will have to create proper secrets in our CI in order to automate it.

```
cat ~/.config/goreleaser/mastodon.sh     
#!/bin/bash
MASTODON_CLIENT_ID=xxx MASTODON_CLIENT_SECRET=yyy MASTODON_ACCESS_TOKEN=zzz goreleaser --clean

~/.config/goreleaser/mastodon.sh
[...]
  • announcing
    • mastodon
      • posting: 'Awesome project 0.0.5 is out!'
      • took: 1s
[...]
```
